<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request){
        $request->validate([
            'name'=>'required',
            'email'=>'required|unique:users,email',
            'phone_number'=>'required|unique:users,phone_number',
            'password'=>'required',
            'confirm_password'=>'required|same:password'
        ]);

        $user = User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone_number'=>$request->phone_number,
            'password'=>Hash::make($request->password),
        ]);
        $token = $user->createToken('user logged in token');
        return response()->json([
            'status' => '200',
            'message' => 'user created successfully',
            'user'=>$user,
            'token'=>$token->accessToken
        ]);

    }

    public function login(Request $request){
        $request->validate([
            'email'=>'required|exists:users,email',
            'password'=>'required'
        ]);

        $user = User::where('email',$request->email)->first();
        $token = $user->createToken('user logged in token');
        if(Hash::check($request->password,$user->password)){
            return response()->json([
                'status'=>200,
                'user'=>$user,
                'token'=>$token->accessToken,
                'messgae'=>'user logged in successfully'
            ]);
        }
        else{
            return response()->json([
                'status'=>'400',
                'message'=>'Invalid password'
            ]);
        }
    }

    public function user_profile(Request $request){
        $user = auth()->user();
        return response()->json([
            'status'=>'200',
            'user'=>$user
        ]);
    }

    public function social_login(Request $request){
        $request->validate([
            'email'=>'required',
        ]);
    }
}
