<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\FriendList;
use App\Models\SocialAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
class FriendController extends Controller
{
    public function add_friend(Request $request){

        $data = $request->validate([
            'name' => 'required|string',
            'username' => 'required|string',
            'dob' => 'required|date',
            'friend_image' => 'required|string',
            // 'account_type' => 'required|string',
            // 'profile_link' => 'required|string',
            // 'profile_username' => 'required|string',
            // 'preferences' => 'required|integer',
        ]);

        // Save image
        // $imagePath = $request->file('friend_image')->store('friend_images','public');
        if (strpos($data['friend_image'], ',') !== false) {
            $data['friend_image'] = explode(',', $data['friend_image'])[1]; // Get the base64 part after comma
        }
        $decodedImage = base64_decode($data['friend_image']);

        // Store the image
        $imagePath = 'friend_images/' . uniqid() . '.jpg'; // Unique filename
        Storage::disk('public')->put($imagePath, $decodedImage);

        // Create FriendList
        $friendList = FriendList::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'dob' => $data['dob'],
            'friend_image' => 'storage/'.$imagePath,
        ]);
        foreach($request->accounts as $account){
            // return $account['type'];
            SocialAccount::create([
                'friend_list_id'=>$friendList->id,
                'type' => $account['type'],
                'username' => $account['username'],
                'profile_link' => $account['profile_link'],
                'preference' => $account['preference'],
            ]);

        }

        return response()->json([
            'status' => '200',
            'message' => 'friend added successfully',
            'friend'=>$friendList,
        ]);

    }

    // public function add_friend_social_account(Request $request){
    //     $data = $request->validate([
    //         'type' => 'required|string',
    //         'profile_link' => 'required|string',
    //         'username' => 'required|string',
    //         'preference' => 'required|integer',
    //     ]);

    //     SocialAccount::create([
    //         'friend_list_id'=>$request->friend_id,
    //     ]);


    // }
}
