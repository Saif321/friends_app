<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('social_accounts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('friend_list_id');
            $table->foreign('friend_list_id')->references('id')->on('friend_lists')->cascadeOnDelete();
            $table->text('icon')->nullable();
            $table->string('type');
            $table->string('username');
            $table->string('profile_link');
            $table->integer('preference');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('social_accounts');
    }
};
