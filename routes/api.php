<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\FriendController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// auth api's
Route::post('/register',[AuthController::class, 'register']);
Route::post('/login',[AuthController::class, 'login']);
Route::post('/social_login',[AuthController::class, 'social_login']);

Route::get('/user_profile',[AuthController::class, 'user_profile'])->middleware('auth:api');
Route::post('/add_friend',[FriendController::class, 'add_friend'])->middleware('auth:api');
Route::post('/add_friend_social_account',[FriendController::class, 'add_friend_social_account'])->middleware('auth:api');
Route::post('/add_logs',[FriendController::class, 'add_friend_social_account'])->middleware('auth:api');


// Route::resource('friend-lists', FriendListController::class);
// Route::resource('friend-lists.social-accounts', SocialAccountController::class)->shallow();

